FROM postgres:9
LABEL image.name="bloonix_postgres" \
      image.description="The container contains the database and the initial data for opensource monitoring software Bloonix." \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2017 epicsoft.de / Alexander Schwarz" \
      license="MIT" \
      org.bloonix.version="0.7-1"

COPY ["./schema-pg.sql", "/docker-entrypoint-initdb.d/schema-pg.sql"]
